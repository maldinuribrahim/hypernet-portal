<?php

namespace Modules\Prtg\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gdevilbat\SpardaCMS\Modules\Core\Http\Controllers\CoreController;

class PrtgController extends CoreController
{
    public function __construct(\Modules\Prtg\Repositories\PrtgRepository $prtg_repository)
    {
        parent::__construct();
        $this->prtg_repository = $prtg_repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->data['sensors'] = $this->prtg_repository->content('sensors')->columns('objid,device,sensor,status,group,totalsens,info=treejson')->getTable(\Auth::user())->sensors;
        $this->data['chart_2_days'] = $this->prtg_repository->query([
                                                        'id' => 0,
                                                        'type' => 'graph',
                                                        'graphid' => 1,
                                                        'graphtitle' => '@@notitle@@',
                                                        'charttext' => '@@period@@',
                                                        'graphstyling' => "baseFontSize='9' showLegend='1'",
                                                        'width' => 1400,
                                                        'height' => 300,
                                                    ])
                                                    ->getChart(\Auth::user());
          $this->data['chart_30_days'] = $this->prtg_repository->query([
                                                        'id' => 0,
                                                        'type' => 'graph',
                                                        'graphid' => 2,
                                                        'graphtitle' => '@@notitle@@',
                                                        'charttext' => '@@period@@',
                                                        'graphstyling' => "baseFontSize='9' showLegend='1'",
                                                        'width' => 1400,
                                                        'height' => 300,
                                                    ])
                                                    ->getChart(\Auth::user());
          $this->data['chart_365_days'] = $this->prtg_repository->query([
                                                        'id' => 0,
                                                        'type' => 'graph',
                                                        'graphid' => 3,
                                                        'graphtitle' => '@@notitle@@',
                                                        'charttext' => '@@period@@',
                                                        'graphstyling' => "baseFontSize='9' showLegend='1'",
                                                        'width' => 1400,
                                                        'height' => 300,
                                                    ])
                                                    ->getChart(\Auth::user());

        return view('prtg::admin.'.$this->data['theme_cms']->value.'.content.master', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('prtg::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('prtg::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('prtg::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
