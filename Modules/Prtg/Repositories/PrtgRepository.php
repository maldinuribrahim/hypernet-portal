<?php

namespace Modules\Prtg\Repositories;

use Gdevilbat\SpardaCMS\Modules\Core\Repositories;
use Gdevilbat\SpardaCMS\Modules\Core\Repositories\AbstractRepository;

use Validator;

/**
 * Class EloquentCoreRepository
 *
 * @package Gdevilbat\SpardaCMS\Modules\Core\Repositories\Eloquent
 */
class PrtgRepository
{
	
    protected $query = [];

    public function content(string $string)
    {
        $this->query['content'] = $string;

        return $this;
    }

    public function columns(string $string)
    {
        $this->query['columns'] = $string;

        return $this;
    }

    public function query(array $query)
    {
        foreach ($query as $key => $value) {
            $this->query[$key] = $value;
        }

        return $this;
    }

    public function getTable(\Gdevilbat\SpardaCMS\Modules\Core\Entities\User $user)
    {
        return json_decode($this->get($user, '/api/table.json'));
    }

    public function getChart(\Gdevilbat\SpardaCMS\Modules\Core\Entities\User $user)
    {
        $body = $this->get($user, '/chart.svg')->getContents();
        $base64 = base64_encode($body);
        $mime = "image/svg+xml";
        $img = ('data:' . $mime . ';base64,' . $base64);

        return $img;
    }

    private function get(\Gdevilbat\SpardaCMS\Modules\Core\Entities\User $user, $api_path)
    {
        $urls = $user->meta->getMetaData('prtg')->url;

        foreach ($urls as $key => $url) {
            $http = new \GuzzleHttp\Client;
            $response = $http->get($url.$api_path.'?'.http_build_query($this->getData($user)));
        }

        $this->reset();

        return $response->getBody();
    }

    private function getData(\Gdevilbat\SpardaCMS\Modules\Core\Entities\User $user)
    {
        $data = [
            'username' => $user->meta->getMetaData('prtg')->username,
            'passhash' => $user->meta->getMetaData('prtg')->passhash,
            'noraw' => 1,
            'count' => 10000
        ];

        $result = array_merge($data, $this->query);

        $validator = Validator::make($result, [
            'content' => 'in:'.implode(',', ['sensors']),
            'columns' => 'string',
            'username' => 'required',
            'passhash' => 'required'
        ]);

        if($validator->fails())
            throw new \Illuminate\Http\Exceptions\HttpResponseException(response()->json($validator->errors(), 422));

        return $result;
    }

    private function reset()
    {
        $this->query = [];
    }
}
