<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'control', 'middleware' => 'core.auth'], function() {

    Route::prefix('prtg')->group(function() {
        Route::group(['middleware' => 'core.menu'], function() {
            Route::get('master', 'PrtgController@index')->middleware('can:menu-prtg')->name('cms.prtg.master');
            Route::get('form', 'PrtgController@create')->name('cms.prtg.form');
            Route::post('form', 'PrtgController@store')->middleware('can:create-prtg');
            Route::put('form', 'PrtgController@store');
            Route::delete('form', 'PrtgController@destroy');
        });

        Route::group(['prefix' => 'api'], function() {
            Route::get('master', 'PrtgController@serviceMaster')->middleware('can:menu-prtg');
        });
    });
         
});