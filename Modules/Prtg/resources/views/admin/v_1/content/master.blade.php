@extends('core::admin.'.$theme_cms->value.'.templates.parent')

@section('title_dashboard', ' PRTG')

@section('breadcrumb')
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
                <a href="#" class="m-nav__link m-nav__link--icon">
                    <i class="m-nav__link-icon la la-home"></i>
                </a>
            </li>
            <li class="m-nav__separator">-</li>
            <li class="m-nav__item">
                <a href="" class="m-nav__link">
                    <span class="m-nav__link-text">Home</span>
                </a>
            </li>
            <li class="m-nav__separator">-</li>
            <li class="m-nav__item">
                <a href="" class="m-nav__link">
                    <span class="m-nav__link-text">PRTG</span>
                </a>
            </li>
        </ul>
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="fa fa-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            PRTG Netwok Monitoring
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-md-5">
                    @if (!empty(session('global_message')))
                        <div class="alert {{session('global_message')['status'] == 200 ? 'alert-info' : 'alert-warning' }} alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            {{session('global_message')['message']}}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="w-100 mb-3">
                    <img class="img-fluid" src="{{$chart_2_days}}" alt="">
                </div>

                <div class="w-100 mb-3">
                    <img class="img-fluid" src="{{$chart_30_days}}" alt="">
                </div>

                <div class="w-100 mb-3">
                    <img class="img-fluid" src="{{$chart_365_days}}" alt="">
                </div>

                <!--begin: Datatable -->
                <table class="table table-striped display responsive nowrap" id="data-prtg" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Sensor</th>
                            <th>Device</th>
                            <th>Status</th>
                            <th>Last Traffic</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sensors as $sensor)
                            <tr>
                                <td>
                                    {{ $sensor->objid }}
                                </td>
                                <td>
                                    {{ $sensor->sensor }}
                                </td>
                                <td>
                                    {{ $sensor->device }}
                                </td>
                                <td>
                                    {{ $sensor->status }}
                                </td>
                                <td>
                                    {{ $sensor->info->data[0]->lastvalue }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>


        </div>

        <!--end::Portlet-->

    </div>
</div>
{{-- End of Row --}}

@endsection

@section('page_script_js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data-prtg').DataTable();
        } );
    </script>
@endsection