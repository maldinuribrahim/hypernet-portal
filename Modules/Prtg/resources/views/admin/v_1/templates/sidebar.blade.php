<li class="m-menu__item  {{Route::current()->getName() == 'cms.prtg.master' ? 'm-menu__item--active' : ''}}" aria-haspopup="true">
    <a href="{{action('\Modules\Prtg\Http\Controllers\PrtgController@index')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-graph"></i>
        <span class="m-menu__link-title"> 
            <span class="m-menu__link-wrap"> 
                <span class="m-menu__link-text">
                    PRTG
                </span>
             </span>
         </span>
     </a>
</li>