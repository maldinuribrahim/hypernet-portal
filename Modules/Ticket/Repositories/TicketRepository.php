<?php

namespace Modules\Ticket\Repositories;

use Gdevilbat\SpardaCMS\Modules\Core\Repositories;
use Gdevilbat\SpardaCMS\Modules\Core\Repositories\AbstractRepository;

use Validator;

/**
 * Class EloquentCoreRepository
 *
 * @package Gdevilbat\SpardaCMS\Modules\Core\Repositories\Eloquent
 */
class TicketRepository
{
    protected $filter;
    protected $id;

    public function filter(Array $array)
    {
        $this->filter = $array;

        return $this;
    }

    public function get()
    {
        $data = [
            'requester_id' => $this->id,
            'include' => 'description',
            'order_by' => 'updated_at',
            'order_type' => 'desc'
        ];

        $validator = Validator::make($data, [
            'requester_id' => 'required',
        ]);

        if($validator->fails())
            throw new \Illuminate\Http\Exceptions\HttpResponseException(response()->json($validator->errors(), 422));

        $http = new \GuzzleHttp\Client;
        $response = $http->get(getSettingConfig('ticket', 'url').'/api/v2/tickets?'.http_build_query($data),[
                'headers' => [
                'Authorization' => $this->getAuth(),
            ]
        ]);

        $this->reset();

        return json_decode($response->getBody());
    }

    public function initId(\Gdevilbat\SpardaCMS\Modules\Core\Entities\User $user)
    {
        $http = new \GuzzleHttp\Client;
        $response = $http->get(getSettingConfig('ticket', 'url').'/api/v2/search/contacts?'.http_build_query(['query' => "\"email:'".$user->meta->getMetaData('ticket')->username."'\""]),[
                'headers' => [
                'Authorization' => $this->getAuth(),
            ]
        ]);

        $this->id = json_decode($response->getBody())->results[0]->id;

        return $this;
    }

    private function reset()
    {
        $this->filter = '';
        $this->id;
    }

    private function getAuth()
    {
        return 'Basic '.base64_encode(getSettingConfig('ticket', 'api_key').':X');
    }

    public function getTicketStatus($number)
    {
        $status = [
            '2' => 'Open',
            '3' => 'Pending',
            '4' => 'Resolved',
            '5' => 'Closed',
        ];

        return $status[$number];
    }
}
