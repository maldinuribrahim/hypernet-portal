<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'control', 'middleware' => 'core.auth'], function() {

    Route::prefix('ticket')->group(function() {
        Route::group(['middleware' => 'core.menu'], function() {
            Route::get('master', 'TicketController@index')->middleware('can:menu-ticket')->name('cms.ticket.master');
            Route::get('form', 'TicketController@create')->name('cms.prtg.form');
            Route::post('form', 'TicketController@store')->middleware('can:create-ticket');
            Route::put('form', 'TicketController@store');
            Route::delete('form', 'TicketController@destroy');
        });

        Route::group(['prefix' => 'api'], function() {
            Route::get('master', 'TicketController@serviceMaster')->middleware('can:menu-ticket');
        });
    });
         
});
