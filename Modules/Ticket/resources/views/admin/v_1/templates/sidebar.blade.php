<li class="m-menu__item  {{Route::current()->getName() == 'cms.ticket.master' ? 'm-menu__item--active' : ''}}" aria-haspopup="true">
    <a href="{{action('\Modules\Ticket\Http\Controllers\TicketController@index')}}" class="m-menu__link ">
        <i class="m-menu__link-icon fa fa-ticket-alt"></i>
        <span class="m-menu__link-title"> 
            <span class="m-menu__link-wrap"> 
                <span class="m-menu__link-text">
                    Ticket
                </span>
             </span>
         </span>
     </a>
</li>